<?php

$filepath = $_GET['f'];

ob_start();
ob_implicit_flush(0);

if (substr($filepath, 0, 1) == '/') {
	http_response_code(404);
	die();
}

if (substr_count($filepath, '.') > 2) {
	http_response_code(404);
	die();
}

if (file_exists($filepath) == false) {
	http_response_code(404);
	die();
}

$ext = pathinfo($filepath, PATHINFO_EXTENSION);

switch($ext) {
	case 'css':
		$mime = 'text/css';
		break;
	case 'js':
		$mime = 'text/javascript; charset=UTF-8';
		break;
	case 'png':
		$mime = 'image/png';
		break;
	case 'woff':
		$mime = 'font/woff';
		break;
	case 'ttf':
		$mime = 'application/font-ttf';
		break;
	case 'eot':
		$mime = 'application/octet-stream';
		break;
	case 'svg':
		$mime = 'image/svg+xml';
		break;
	default:
		http_response_code(404);
		die();
		break;
}

$cachefile = '/tmp/cache/' . $filepath. '.gz';
if (!file_exists('/tmp/cache/')) {
	mkdir('/tmp/cache/', 0777);
	mkdir('/tmp/cache/immagini', 0777);
	mkdir('/tmp/cache/fonts', 0777);
}
$cachetime = 1296000; // time to cache in seconds

if (file_exists($cachefile) && time() - $cachetime <= filemtime($cachefile)) {
	$contents = file_get_contents($cachefile);
} else {
	unlink($cachefile);
	$contents = file_get_contents($filepath);
	$size = strlen($contents);
	$contents = gzcompress($contents, 9);
	$contents = substr($contents, 0, $size);
	file_put_contents($cachefile, $contents);
}

//header("Access-Control-Allow-Origin: *");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=" . basename($filepath));
header("Content-Type: " . $mime);
header("Content-Transfer-Encoding: binary");
header("Content-Encoding: gzip");
header("Cache-Control: max-age=2592000");
// header('Content-Length: ' . filesize($cachefile);

print("\x1f\x8b\x08\x00\x00\x00\x00\x00");
echo $contents;
exit();
